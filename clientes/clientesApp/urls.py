from django.urls import path
from .views import *

urlpatterns = [
    path('list/', client_list, name='client_list'),
    path('new/', client_new, name="client_new"),
    path('update/<int:id>/', client_update, name='client_update'),
    path('update/<int:id>/', client_delete, name='client_delete'),

]